const assert = require('assert');
const utils = require('tramway-core-testsuite');
const lib = require('../dist/index.js');
var describeCoreClass = utils.describeCoreClass;
var describeFunction = utils.describeFunction;

describe("Simple acceptance tests to ensure library returns what's promised.", function(){
    describe("Should return a proper 'DICommandFactory' class", describeCoreClass(
        lib.default, 
        "DICommandFactory", 
        [],
        ["create"],
    ));
});