import {DependencyResolver} from 'tramway-core-dependency-injector';
import { CommandNotFoundError } from './errors';

export default class DICommandFactory {
    constructor(commands) {
        this.commands = commands;
    }

    create(commandName) {
        const service = this.commands[commandName];

        if (!service) {
            throw new CommandNotFoundError(commandName);
        }
        
        return DependencyResolver.getService(service);
    }
}