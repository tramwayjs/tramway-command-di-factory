export default class CommandNotFoundError extends Error {
    constructor(command) {
        super(`The following command ${command} does not have a service attached to it. Check if you made a typo in the CLI or if your services declaration for the command is properly mapped.`)
    }
}