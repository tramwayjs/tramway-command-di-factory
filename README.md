Tramway Command Dependency Injector Factory is a plugin for `tramway-command` that lets you configure your commands configured with `tramway-core-dependency-injector`. It includes:

1. The DICommandFactory class

# Installation:
1. `npm install tramway-command-di-factory`

# Example project:
https://gitlab.com/tramwayjs/tramway-command-example

# Documentation

## Recommended Folder Structure
- config/parameters/global
- config/services
- commands

## Implementing with Dependency Injection
With the tramway-command-di-factory, you are able to use the Dependency Injection library to build and manage your commands and Command Resolver setup.

### Setup

PROJECT_ROOT / src / config / services / core.js

```javascript
import CommandResolver from 'tramway-command';
import DICommandFactory from 'tramway-command-di-factory';

export default {
    "app": {
        "class": CommandResolver,
        "constructor": [
            {"type": "parameter", "key": "commands"},
            {"type": "service", "key": "factory.command"},
        ],
    },
    "factory.command": {
        "class": DICommandFactory,
        "constructor": [
            {"type": "parameter", "key": "commands"},
        ]
    }
}
```

### Register your commands as services

PROJECT_ROOT / src / config / services / commands.js

```javascript
import {TestCommand} from '../../commands';

export default {
    "command.test": {
        "class": TestCommand,
        "constructor": [],
    }
}
```

### Add a mapper of command cli to command service to the parameters

PROJECT_ROOT / src / config / parameters / global / commands.js

```javascript
export default {
    "test:command": "command.test",
}
```

### Have the runner file get the CommandResolver from the dependency injection container and run it

PROJECT_ROOT / src / COMMAND_RUNNER_FILE.js

```javascript
import {DependencyResolver, dependencies} from 'tramway-core-dependency-injector';
import * as parameters from './config/parameters';
import services from './config/services';

const {ParametersManager, ServicesManager} = dependencies;
DependencyResolver.create(new ServicesManager(), new ParametersManager()).initialize(services, parameters);

DependencyResolver.getService('app').run();
```